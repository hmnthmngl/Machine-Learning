import numpy as np
import pandas as pd
import math
# from sklearn.linear_model import LinearRegression


def predict_using_sklean():
    df = pd.read_csv("test_scores.csv")
    r = LinearRegression()
    r.fit(df[['math']],df.cs)
    return r.coef_, r.intercept_

def gradient_descent(x,y):
    iterations = 1000000
    learning_rate = 0.0002
    b = m = 0
    n = len(x)
    cost_previous = 0

    for i in range(iterations):
        cost = (1/n) * sum([val**2 for val in (y-(m*x+b))])
        bd = -(2/n)*sum(x*(y-(m*x+b)))
        md = -(2/n)*sum(y-(m*x+b))
        b = b - learning_rate * bd
        m = m - learning_rate * md

        if math.isclose(cost, cost_previous, rel_tol=1e-20):
            break
        cost_previous = cost
        print ("m {}, b {}, cost {}, iteration {}".format(m,b,cost, i))
    return m, b


x = np.array([92,56,88,70,80,49,65,35,66,67])
y = np.array([98,68,81,80,83,52,66,30,68,73])



m, b = gradient_descent(x,y)
print("Using gradient descent function: Coef {} Intercept {}".format(m, b))

# m_sklearn, b_sklearn = predict_using_sklean()
# print("Using sklearn: Coef {} Intercept {}".format(m_sklearn,b_sklearn))
